/// <reference types="cypress"/>


describe('Tela Home', () => {

    beforeEach(() => {
        cy.visit('')
    })

    it('Deve acessar a tela de Home', () => {

        cy.get('#__next').should('be.visible');
        cy.get('div[class="swiper-container swiper-container-horizontal"]').should('be.visible');
    });

    it('Acessar tela de login',() => {
        cy.get('button[class="btn-user"]').click();
        cy.get('div[class="btn-entrar"]').click();
        cy.get('div[class="container"]').should('be.visible');
    });

    it('Realizar pesquisa por produto',() =>{
        cy.get('input[name="busca"]').type('Celular{enter}',{force:true})
        cy.get('button > strong').should('contain.text', 'Celular')
    })

    it('Cadastrar email para receber promoções', () => {
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div/form/input[1]')
            .type('Teste automatizado',{force:true});
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div/form/input[2]')
            .type('teste@teste'+ Math.floor(Math.random()*10)+'.com{enter}', {force:true});
        cy.get('#__next').should('contain.text','Cadastro realizado com sucesso!')
        cy.get('#NewsletterToast').should('contain.text','Cadastro realizado com sucesso!')
    })

    it('Email já cadastrado para receber promoções',() => {
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div/form/input[1]').
            type('Teste automatizado',{force:true});
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div/form/input[2]')
            .type('teste@teste.com{enter}', {force:true});

        cy.get('#__next')
            .should('contain.text','Esse email já está cadastrado para receber nossas ofertas exclusivas.')
        cy.get('#NewsletterToast')
            .should('contain.text','Esse email já está cadastrado para receber nossas ofertas exclusivas.')
    })

    it('Navegando pelos sub-menus', () =>{
        cy.get('button[class="btn"]').trigger('mouseover');
        cy.xpath('//*[@id="__next"]/div/section[1]/div/div/div[1]/ul/li[1]/a')
            .click(); 
        cy.get('button > strong').should('contain.text', 'ar-e-ventilacao')

        cy.get('button[class="btn"]').trigger('mouseover');
        cy.xpath('//*[@id="__next"]/div/section[1]/div/div/div[1]/ul/li[2]/a')
            .click(); 
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[2]/div[3]/div[2]/ul/li/button/strong')
            .should('be.visible')
            .and('contain.text','beleza-e-saude')
    })
});
