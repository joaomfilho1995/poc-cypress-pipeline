/// <reference types="cypress" />

describe('Teste API', () => {

    it('Endpoint traga o produto', () => {
        cy.request({
            method: 'GET',
            url:'https://staging-marketplace-api.gazin.com.br/v1/canais/produtos/596?cep=87005110',
            headers:{
                'canal': 'gazin-ecommerce'
            }
        }).then(response => {
            expect(response.body).to.not.be.null;
            const { body } = response;
            expect(body.id).to.eq(596);
            expect(body).to.have.property('variacoes');
            const {variacoes} = body;
            expect(variacoes).to.not.be.empty;     
        });
    });
})