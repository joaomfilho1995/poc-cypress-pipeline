/// <reference types="cypress" />

describe('Carrinho', () => {

    beforeEach(() => {
        cy.visit('');
        
    })

    it('Adicionar Celular no carrinho', () => {
        cy.get('.entendi').click({ force: true });

        cy.get('input[name="busca"]').type('celulares {enter}');
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[2]/div[2]/div[3]/div/div/a[1]/div/div[2]/h3').click({ force: true });
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/h2')
            .should('contain.text', 'Smartphone Samsung Galaxy A10s 6.2" Octa Core 32 GB 2GB Câmera Dupla - Azul - Quadriband');

        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[2]/div/input').type('87005-110');
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[2]/div/button').click({ force: true });

        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[3]/div[2]/div[1]/div[1]/span')
            .should('contain.text', 'Entrega Convencional');

        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[3]/div[2]/div[2]/div[1]/span')
            .should('contain.text', 'Entrega Rápida');

        //comprar
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[2]/button[1]').click({ force: true });

        cy.xpath('//*[@id="__next"]/div/main/div/div[2]/div[1]/div[1]/span')
            .should('contain.text', 'Carrinho de compras (1)');

        cy.xpath('//*[@id="__next"]/div/main/div/div[2]/div[1]/div[2]/div[2]/div/div[1]/div[2]/a/h4')
            .should('contain.text', 'Smartphone Samsung Galaxy A10s 6.2" Octa Core 32 GB 2GB Câmera Dupla - Azul - Quadriband');
    });


    it('Que o valor do frete seja o mesmo que o pesquisado (entrega convencional)', () => {
        cy.get('.entendi').click({ force: true });

        cy.get('input[name="busca"]').type('Smartphone Samsung Galaxy A10s {enter}',{force:true})

        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[2]/div[2]/div[3]/div/div/a[1]/div/div[2]/h3').click({ force: true });
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/h2')
            .should('contain.text', 'Smartphone Samsung Galaxy A10s 6.2" Octa Core 32 GB 2GB Câmera Dupla - Azul - Quadriband');

        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[2]/div/input').type('87005-110');
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[2]/div/button').click({ force: true });

        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[3]/div[2]/div[1]/div[1]/span')
            .should('contain.text', 'Entrega Convencional');

        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[3]/div[2]/div[1]/div[2]/span')
            .then( value => {
                const valorFrete = value.text();
                cy.xpath('/Brasil79081/*[@id="__next"]/div/main/section[2]/div[1]/div[2]/div[3]/div/div[1]/ul/li[1]/div[2]/div/span[1]')
                    .should('contain.text',`Frete: ${valorFrete}`);
        });
    });

    it.only('Removendo um item do carinho',() =>{
        cy.get('.entendi').click({ force: true });

        cy.get('input[name="busca"]').type('celulares {enter}');
        cy.xpath('//*[@id="__next"]/div/main/section[2]/div[2]/div[2]/div[3]/div/div/a[1]/div/div[2]/h3').click({ force: true });
        cy.comprar()
        
        //cy.get('button[class="btn-continuar"]').click();

        cy.get('button[class="btn-show-cart"]').trigger('mouseover')
        cy.xpath('//*[@id="__next"]/div/header/header/div[3]/div/div[2]/div[5]/div/div/div[1]/div/div[2]/button/img').click();
        cy.get('#__next').should('contain.text','Produto removido!');

        cy.get('div[class="Toastify"]').should('contain.text','Produto removido!');
        cy.get('div[class="container"]').should('be.visible').and('contain.text','Ops! Nenhum produto no carrinho');
        
    })
})