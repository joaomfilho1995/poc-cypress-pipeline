/// <reference types="cypress" />


beforeEach(() => {
    cy.visit('')
    cy.login('teste007@uorak.com','12345678')
    cy.visit('/conta/enderecos')
})

describe('Cadastro e edição de endereço',() =>{
    it('Cadastrar um novo endereço para usuário',() => {

        cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div[1]/div/div/button')
            .click({force:true})

        cy.get('#destinatario_nome')
            .clear()
            .type('Teste Automatizado I')

        cy.get('input[name="telefone_completo"]')
            .type('67999999999')

        // cy.intercept('GET','**/ws/79081650/json')  
        //     .as('spyCep')

        cy.get('input[name="cep"]').type('79081350')

        /*cy.wait('@spyCep').its('response.body.uf')
            .should('eq', 'MS')
        */

        cy.get('input[name="numero"]').type('150')

        cy.get('button[class="centralCliente__ButtonCadastrar-sc-1yj8frw-3 cGitGC"]')
            .click()

        cy.get('div[class="Toastify__toast Toastify__toast--success"]')
            .should('be.visible')
            .and('contain.text','Endereço cadastrado com sucesso!')
    })


    it.only('Deve alterar o endereço do usuário, esperando mensagem de confirmaão',() => {

        cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div[2]/div/div/button') 
            .click({force:true})

        cy.get('#destinatario_nome')
            .clear()
            .type('Teste cadastro endereço')

        cy.intercept('GET','**/ws/79081650/json')  
            .as('spyCep')

        cy.get('input[name="cep"]') 
            .clear()
            .type('79081650')

        cy.wait('@spyCep').its('response.body.uf')
            .should('eq', 'MS')
      
        cy.get('input[name="numero"]')
            .clear()
            .type('007')

        cy.get('input[name="logradouro"]')
            .invoke('val')
            .as('rua')


        cy.get('input[name="bairro"]')
            .invoke('val')
            .as('bairro')
        
            
        cy.get('input[name="cidade"]')
            .invoke('val')
            .as('cidade')


        cy.get('button[class="centralCliente__ButtonCadastrar-sc-1yj8frw-3 cGitGC"]')
            .click()


        cy.get('div[class="Toastify__toast Toastify__toast--success"]')
            .should('be.visible')
            .and('contain.text','Endereço alterado com sucesso!')

        cy.get('@rua').then(rua => {
            cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div[2]/div/div/div/span[1]')
                .should('contain.text',`${rua}` + ', '+ '007')
        })
    

        cy.get('@bairro').then(bairro => {
            cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div[2]/div/div/div/span[2]')
                .should('contain.text', `${bairro}` + ' - 79081650')
        })

        cy.get('@cidade').then(cidade => {
            cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div[2]/div/div/div/span[3]')
                .should('contain.text', `${cidade}` + ' / MS')
        })
   })
})
