/// <reference types="cypress" />
const faker = require('faker-br');

let usuario = {
    nome: faker.name.firstName(),
    email: faker.internet.email(),
    dataNascimento: '01011995',
    cpf: faker.br.cpf(),
    telefone: '99999999999',
    senha: '123mudar',
    confirSenha: '123mudar'
}
beforeEach(() => {
    cy.visit('')
})


describe('Cadastro Usuário', () => {

    it('usuario cadastrado com sucesso', () => {
        cy.visit('/cadastro');
        cy.get('input[name="nome"]').type(usuario.nome);
        cy.get('input[name="email"]').type(usuario.email);
        cy.get('input[name="cpf"]').type(usuario.cpf);
        cy.get('input[name="data_nascimento"]').type(usuario.dataNascimento);
        cy.get('input[name="telefone"]').type(usuario.telefone);

        cy.get('input[name="sexo"]').check({ force: true });
        cy.get('input[name="password"]').type(usuario.senha);
        cy.get('input[name="password_confirmation"]').type(usuario.confirSenha);
        cy.xpath('//*[@id="__next"]/div/main/div/div/form/div[10]/button').click({ force: true });

        cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div/div[1]/h3').should('contain.text','Minha Conta');
        cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div/div[1]/span[1]').should('contain.text', usuario.nome);
    });

    const editarUsuario = (nome,telefone,dataNasc) => {
        cy.visit('/conta')
        
        cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div/div[1]/button')
            .click();
    
        cy.get('input[name="nome"]')
            .clear()
            .type(nome)
        
        cy.get('input[name="telefone"]')
            .clear()
            .type(telefone)
        
       cy.get('input[name="data_nascimento"]')
            .clear()
            .type(dataNasc)

        cy.get('button[class="centralCliente__ButtonCadastrar-sc-1yj8frw-3 cGitGC"]')
            .click()
    }


    it('Deve alterar dados do usuário, esperando mensagem de confirmação',() =>{
 
        const nome = 'Teste Automatizado'
        const telefone = '(67) 99999-9999'
        const dataNasc = '01/01/1987'

        cy.login('teste007@uorak.com','12345678')
        editarUsuario(nome,telefone,dataNasc)

        cy.get('div[class="Toastify__toast-body"]')
            .should('be.visible')
            .and('contain.text','Dados alterados')

        cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div/div[2]/div[1]/div[2]')
            .should('to.visible')
            .and('contain.text',nome)

        cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div/div[2]/div[4]/div[2]')
            .should('be.visible')
            .and('contain.text',telefone)

    });


    it('Deve alterar email do usuário e mostrar mensagem de confirmação',() => {

        const novoEmail = 'teste007@uorak.com'
        const senha = "12345678"

        cy.login('teste007@uorak.com','12345678')
        cy.visit('/conta')

        cy.xpath('//*[@id="__next"]/div/main/div/div/div[1]/div[1]/div[1]/ul/li[2]/div/a')
            .click()

        cy.get('input[name="novo_email"]').type(novoEmail)
        cy.get('input[name="novo_email_confirmacao"]').type(novoEmail)
        cy.get('input[name="password"]').type(senha + '{enter}')

        cy.get('div[class="Toastify__toast-body"]')
            .should('be.visible')
            .and('contain.text','Email alterado com sucesso!')
        
        cy.xpath('//*[@id="__next"]/div/main/div/div/div[2]/div/div[1]/span[2]')
            .should('equal',novoEmail)
    })

    
    it('Deve alterar senha do usuário, esperando mensagem de confirmação',() => {
        const email = 'teste007@uorak.com'
        const senhaAntiga = '12345678'
        const senhaNova = '12345678'

        cy.login('teste007@uorak.com','12345678')

        cy.visit('/conta')
        cy.xpath('//*[@id="__next"]/div/main/div/div/div[1]/div[1]/div[1]/ul/li[3]/div/a')
            .click()

        cy.get('input[name="senha_antiga"]').type(senhaAntiga)
        cy.get('input[name="senha_nova"]').type(senhaNova)
        cy.get('input[name="senha_nova_confirmacao"]').type(senhaNova + '{enter}')

        cy.get('#__next')
            .should('contain.text','Senha alterada com sucesso!')
    })


    it('Deve esperar mensagem de erro ao inserir senha errada', () => {
        const email = 'teste007@uorak.com'
        const senhaAntiga = '1234567899'
        const senhaNova = '12345678'

        cy.login('teste007@uorak.com','12345678')

        cy.visit('/conta')
        cy.xpath('//*[@id="__next"]/div/main/div/div/div[1]/div[1]/div[1]/ul/li[3]/div/a')
            .click()

        cy.get('input[name="senha_antiga"]').type(senhaAntiga)
        cy.get('input[name="senha_nova"]').type(senhaNova)
        cy.get('input[name="senha_nova_confirmacao"]').type(senhaNova + '{enter}')

        cy.get('#__next')
            .should('contain.text','A senha atual está incorreta.')

    })
});