// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
 
Cypress.Commands.add("login", (email,senha) => { 
    
    cy.visit('/login');
        
    /*cy.xpath('//*[@id="__next"]/div/header/header/div[3]/div/div[2]/div[4]/div/div')
        .click({force: true});
    */
    cy.xpath('//*[@id="__next"]/div/main/div/div[2]/form/div[2]/div[2]/input')
        .type(email);
    
    cy.xpath('//*[@id="__next"]/div/main/div/div[2]/form/div[3]/div[2]/input')
        .type(senha +'{enter}');
 })


 Cypress.Commands.add("comprar",() =>{
    cy.get('button[class="btn-continuar"]').click();
 })

//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
