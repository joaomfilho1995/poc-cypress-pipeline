# (POC) Pipeline - Cypress - QA

teste123
Este projeto foi criado para o desenvolvimento de uma POC, referente ao Cypress com a utilização de pipelines no Gitlab.

# Etapas da Pipeline

Nesta POC também foi criado ilustrativamente uma possível pipeline completa de desenvolvimento. As etapas representadas aqui são:

- **Tests:** Engloba todos os testes de desenvolvimento do projeto, estes testes podem ser por exemplo unitários que o desenvolvedor escreve.

- **Sonar:** Valida naquela determinada branch se há um aumento de bugs, code smells e diminuição da cobertura de testes.

- **Build:** Realiza toda a build necessário para que o sistema funcione, neste caso exemplificado seria a realização da build do frontend.

- **Deploy:** Esse estágio foi dividido em duas etapas, uma de deploy do ambiente de desenvolvimento, e outro para o deploy para o ambiente de qualidade, dessa forma há uma separação completa dos ambientes e também uma independência entre dev e QA, visto que com apenas um clique há o deploy no sistema.

- **Qualidade:** Estágio que executa os testes automatizados *end-to-end*, utilizando o Cypress. Foi adicionado quase no final das etapas, pois no futuro pode ser utilizado o teste *end-to-end* para validar antes que realize o deploy na produção.

Para verificar intuitivamente a pipeline, clique no seguinte [link](https://gitlab.gazin.com.br/tig-mga/poc-cypress-pipeline/-/pipelines/7228).


# Instalação e execução do projeto.

Antes do download do projeto é necessário a instalação do Node na versão mais recente. 

1. Realizar o download do projeto, pode ser realizado pelo botão de **download** ou realizando um **clone** do projeto. 

2. Após o download, dentro da pasta do projeto, abra o *git bash* ou o *cmd* execute o comando `npm install`

3. Ao término do comando anterior, basta executar o comando `npx cypress open`, que estará abrindo uma interface com todos os testes que foram criados. Para executar o teste, basta clicar no teste específico. 


# Informações Adicionais

## Básico de Git e GitHub

O Git é um sistema open-source de controle de versão utilizado pela grande maioria dos desenvolvedores atualmente. Com ele podemos criar todo histórico de alterações no código do nosso projeto e facilmente voltar para qualquer ponto para saber como o código estava naquela data.

Além disso, o Git nos ajuda muito a controlar o fluxo de novas funcionalidades entre vários desenvolvedores no mesmo projeto com ferramentas para análise e resolução de conflitos quando o mesmo arquivo é editado por mais de uma pessoa em funcionalidades diferentes.

### Funcionamento

Tudo no Git é movido através dos pontos na história do projeto que são chamados de commits, esses pontos são formados por conjuntos de alterações em um ou mais arquivos e somados a um descritivo que resume as alterações nesse ponto.

De forma prática, pensando que tenhamos que desenvolver um sistema de login completo, nossos commits podem ficar dessa forma:

1. Configuração da estrutura do projeto
2. Estrutura da página de login
3. Estilos CSS da página de login
4. Estrutura da página de cadastro
5. Resolvido problema no login
6. Estilos CSS da página de cadastro

Veja que nossos commits descrevem exatamente as alterações que o código sofreu e além do título podemos detalhar ainda mais com um texto maior.

É muito importante essas informações estarem bem completas para que todos do time (inclusive você no futuro) possam entender o que foi feito nesse ponto.

### Formato de Ramificações 

Imagine que você esteja trabalhando no meio de uma grande funcionalidade, pode levar até 2 meses para terminá-la. Em uma bela manhã de sol seu chefe resolve pedir urgentemente uma alteração na versão em produção da aplicação, ou seja, você não pode utilizar o código em que está trabalhando pois o mesmo possui features inacabadas. Como resolver?

As ramificações ou *branchs* no Git são formas de termos uma mesma versão do código sofrendo alterações e recebendo *commits* de diferentes fontes e inclusive por diferentes desenvolvedores.

Dessa forma, nós podemos manter um ramo para nossa funcionalidade que irá levar mais tempo e trabalhar em outro *branch* com a versão em produção para realizar alterações mais urgentes. E fica tranquilo, no fim de tudo o Git ainda vai nos ajudar a unir os códigos desses dois ramos de forma muito simpática.

Por padrão, você sempre está trabalhando em um ramo no Git, e mesmo quando você não cria um *branch*, o Git cria automaticamente um *branch* chamado master como padrão.

Na imagem abaixo podemos ver um exemplo de trabalho com vários ramos e commits aplicados. Veja que em alguns pontos da história os ramos são unidos para que as alterações de um ramo sejam aplicadas a outro.

![ramificacoes](./img/branchs.png)

Nesse caso, *“master”*, *“Hotfix”*, *“Release”*, *“Develop”* e os *“Feature”* são os ramos enquanto que os círculos são os *commits*. As caixas com v0.1, v0.2 e v1.0 são versões (conhecidas por tags) que foram pra versão em produção e podem ser compostas por pontos na história de vários branchs.

## Principais comandos do Git

`git init` : iniciando git na pasta do projeto (só deve ser feito quando você criar um projeto novo); 

`git add nome-do-arquivo-incluindo-extensão`: adicionando os arquivos no estágio de controle;

`git add .`: adicionar todos os arquivos do diretório;

`git status`: saber o status do projeto, verificar quais arquivos estão alterados, quais já foram adicionados e deletados;

`git reset HEAD nome-do-arquivo`: voltando ao estágio anterior do adicionamento;

`git commit -m "Mensagem do commit"`: Realizando um *commit*;

`git reset HEAD~1`: voltar um *commit* (CUIDADO);

`git reset HEAD~2`: voltar dois *commits* (CUIDADO);

`git reset HEAD~1 --soft`: voltando um commit e deixando o arquivo no estagio anterior (CUIDADO);

`git reset HEAD~1 --hard`: Voltando um commit e excluindo o arquivo, deixando no estágio anterior (MUITO CUIDADO);

`git checkout -b nome-do-branch`: criando uma *branch*;

`git branch`: verificando qual *branch* está;

`git checkout master`: voltando para a *branch* master;

`git branch -D nome-do-branch`: removendo uma *branch*

`git rm nome-do-arquivo`: deletando arquivos

`git pull`: atualizando a *master* (quando você já está na *branch* master)

`git pull origin master`: atualizando a *branch* que está atualmente com a *branch* master;

`git push -u origin nome-branch`: criando um *merge request* para a *master*;

`git clone url_projeto`: realizando o clone de um projeto existente.

# Referências

- [Git - guia prático](https://rogerdudler.github.io/git-guide/index.pt_BR.html)

- [Documentação - Cypress](https://docs.cypress.io/guides/overview/why-cypress.html)

- [Cypress-xpath](https://github.com/cypress-io/cypress-xpath)

- [Cypress-testing-library](https://github.com/testing-library/cypress-testing-library)

- [Faker-br](https://github.com/tamnil/faker-br)
